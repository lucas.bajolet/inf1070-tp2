# Statistiques d'un répertoire 

Dans cette question, vous devez créer un script nommé `statistique` qui permet d'afficher
un ensemble d'informations sur un répertoire.

## Description du fonctionnement et exemples 

Le script `statistique` s'exécute avec le chemin d'un répertoire (relatif ou absolu) et il affiche les informations suivantes,

* **Nombre de répertoires** : le nombre de sous-répertoires dans le répertoire donné en argument. On ne compte 
pas le répertoire lui-même.
* **Nombre de répertoires vides** : si le répertoire donné en paramètre contient des répertoires vides, le 
script affichera une ligne supplémentaire pour indiquer le nombre de répertories vides. Si non, aucune ligne 
supplémentaire n'est affichée.
* **Nombre de fichiers** : le nombre de tous les fichiers contenu dans le répertoire donné en argument, ainsi 
que dans tous ses sous-répertoires. On entend par *Fichier* un *fichier régulier Linux*.
* **Nombre de liens symboliques** : le nombres de liens symboliques dans le répertoire donné en argument ainsi 
que dans tous ses sous-repertoires.
* **Taille** : donne la taille totale du répertoire donné en paramètre.

Par exemple, si le chemin donné est un répertoire avec l'arborescence suivante, 
```
TP2/
├── liste_etudiants.csv
├── notes/
│   └── notes -> ../liste_etudiants.csv
├── remise/
└── solutions/
    ├── solutions_alternatives.sh
    └── solutions.sh
```

alors l'exécution `statistique` avec l'argument `TP2` affichera les informations suivantes,
```
./statistique TP2/
Statistiques du répertoire : TP2/
    - Nombre de répertoires : 3
        - Nombre de répertoires vides : 1
    - Nombre de fichiers : 3
    - Nombre de liens symboliques : 1
    Taille : 92K
```
Si aucun chemin n'a été précisé c'est le répertoire courant `.` qui sera considéré.

Si le chemin donné n'est pas celui d'un répertoire, le message suivant est affiché
```
./statistique fichier.txt
fichier.txt n'est pas un répertoire!
```
Si le répertoire donné en paramètre n'existe pas alors le message suivant est affiché
```
./statistique les_licornes_existent/
les_licornes_existent/ aucun répertoire avec ce nom!
```

## Exemples d'exécution
Pour l'arborescence suivante,
```
./
├── liste_de_courses
├── depenses_mensuelle
└── economie_totale
```
l'exécution du script donnera
```
./statistique
Statistiques du répertoire : ./
    - Nombre de répertoires : 0
    - Nombre de fichiers : 3
    - Nombre de liens symboliques : 0
    Taille : 30K
```

Pour l'arborescence suivante,
```
MAT7560/
├── bibliographie.bib
├── devoirs/
│   └── devoir_1/
│       ├── bibliographie.bib -> ../../bibliographie.bib
│       ├── devoir-1.tex
│       └── recherche_locale.py
├── presentations/
│   ├── presentation_1/
│   │   ├── bibliographie.bib -> ../../bibliographie.bib
│   │   └── presentation_1.tex
│   └── presentation_2/
├── rapports/
│   ├── bibliographie.bib -> ../bibliographie.bib
│   └── projet-desc.tex
└── tikz/
    ├── exec-1.tikz
    ├── exec-2.tikz
    ├── exec-3.tikz
    ├── exec-4.tikz
    ├── exec-5.tikz
    ├── exemple-1.tikz
    ├── exemple-2.tikz
    ├── exemple-3.tikz
    ├── exemple-4.tikz
    ├── exemple-5.tikz
    ├── exp-1.tikz
    ├── exp-2.tikz
    ├── exp-3.tikz
    ├── exp-4.tikz
    ├── exp-5.tikz
    ├── exp-6.tikz
    ├── exp-7.tikz
    ├── exp-8.tikz
    └── struct.tikz
```
le script affichera 
```
./statistique
Statistiques du répertoire : MAT7560/
    - Nombre de répertoires : 7
        - Nombre de répertories vides : 1
    - Nombre de fichiers : 24
    - Nombre de liens symboliques : 3
    Taille : 524K
```

## Contraintes
L'affichage doit être identique aux exemples plus haut. Le formatage de la sortie du script
est la suivante,
* Aucune indentation pour la première ligne `Statistiques du répertoire`.
* Une indentation et un tiret `-` sont appliqués à toutes les lignes qui suivent sauf pour la 
ligne `Taille` qui est indentée mais qui n'a pas de tiret.
* Une indentation supplémentaire est appliquée avant le tiret pour la ligne `Nombre de répertories vides`
s'il y a lieu.
