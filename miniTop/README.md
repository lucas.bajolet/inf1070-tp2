# Recoder une mini version du programme "top"

Pour cette question, vous devez créer un script nommé `minitop.sh` qui affiche le résultat de la commande `ps` en boucle et avec un rafraichissement de 1 seconde.

## Gestion d'erreurs
minitop accepte un paramètre optionnel :
- un chiffre qui représente le nombre de lignes à afficher
- Si ce chiffre vaut `0` ou moins, on ne veut pas de boucle ni de rafraichissement, mais simplement un appel au programme ps, qui affichera seulement 10 lignes.
- Si ce chiffre n'est pas spécifié, 5 lignes seront affichées.
- Si le paramètre n'est pas un chiffre, ou bien un chiffre supérieur à 25, le message suivant sera affiché :
`Minitop: Entrez un paramètre numérique inférieur ou égal à 25.`

## Exemples
```sh
camille@bash: ./minitop.sh
camille   3551  0.0  1.4 00:00:47 auto-multiple-c
camille   1629  0.0  0.4 00:00:56 ibus-daemon
gdm        933  0.0  3.9 00:01:33 gnome-shell
camille   9450  0.6 24.2 00:02:59 gnome-shell
camille   1441  0.4  5.6 00:09:24 Xorg
```

## Fonctionnement
minitop contient une "boucle infinie" qui lui permet de rester en cours d'execution. Toutes les 1 seconde l'appel à `ps` est renouvelé et l'affichage des nouvelles lignes se fait par dessus les anciennes lignes. Visuellement cela reste presque statique, comme l'appel au programme "top".

On peut quitter le programme en utilisant simplement Ctrl-C.

L'appel à `ps` doit être fait de la manière suivante :
- afficher tous les processus du système
- afficher les colonnes suivantes :
le nom d'utilisateur, le pid, l'utilisation du cpu, le pourcentage de la mémoire physique que le process occupe, le temps CPU cumulé, et le nom de la commande.

Les options à afficher sont les mêmes en cas d'option sans la boucle infinie.

Pour afficher par dessus des lignes déjà écrites, vous allez avoir besoin de la page de manuel "console_codes" et de caractères spéciaux. Cherchez le paragraphe qui contient "CSI sequence".

## Exemples
Pour mieux comprendre comment utiliser ces caractères spéciaux, testez ça :
```sh
echo -e "coucou___  \033[4Di"
echo -e "coucou___  \033[1Di"
echo -e "coucou___  \033[10DQ"
echo -e "coucou___  \033[13Cplus loin: bonjour"
```
