# Réaliser un petit script qui change l'aspect de votre prompt

À l'aide de la page de manuel de "bash" section "prompting"
Vous allez modifier la variable d'environnement `PS1` qui contrôle le prompt.

Votre fichier s'appelera `miniprompt` et fonctionnera de la manière suivante:

## Exemple
```sh
camille@vm:~ source ./miniprompt.sh
Sat Mar 23@bash:
```
Que s'est il passé ?
La commande `source` indique au shell de charger la variable PS1 dans l'environnement courant, et donc de modifier live l'apparence de votre prompt.

## Format
Votre fichier prompt sera sous la forme
```sh
PS1="contenu du nouveau prompt"
```
Et c'est à vous de lire le manuel pour trouver le contenu à mettre dans la variable. Le résultat doit être comme dans la dernière ligne de l'exemple plus haut.
