# Recoder une version du programme "tac"

Pour cette question, vous devez créer un script nommé `minitac.sh` qui copie un fichier en le renversant.

Deux paramètres sont nécéssaires à minitac :
- `fichier source`: qui est le fichier à renverser
- `fichier cible`: qui recevra le résultat du fichier renversé

## Gestion d'erreurs
- Si un de ces deux paramètres est manquant, minitac affiche (sur la sortie d'erreur) le message suivant :

```sh
Minitac: Usage: ./minitac.sh fichierSource fichierRenverse
```
puis l'exécution se termine avec un code d'erreur "1"

- Si le fichier source passé en argument n'existe pas, minitac affiche (sur la sortie d'erreur) le message d'erreur suivant :
```sh
Minitac: Le fichier source est introuvable.
```
puis l'exécution se termine avec un code d'erreur "1"

- Si le fichier destination n'existe pas, minitac le crée silencieusement.
Il écrase son contenu systématiquement.


## Exemple

```sh
cat monFichier
poule
oeuf
terre

./minitac.sh monFichier reverb

cat reverb
terre
oeuf
poule
```
