# metarkane

Pour cet exercice, vous devrez écrire une variante de générateur de mission 19 du tp1.

Le concept est de prendre du texte, le couper en parties égales, et le séparer dans des fichiers triés alphabétiquement.

Chaque partie de la phrase devra être placé à la fin du fichier généré, dans lequel vous aurez préalablement écrit 250 caractères aléatoires.

## Manuel

Vous devrez réaliser un script shell appelé `metarkane.sh`.

Il devra respecter l'utilisation suivante:

`./metarkane.sh <word_src> <taille_partie_texte> <out_dir> <texte>...`

Lorsqu'un utilisateur entre des paramètres invalides, le message d'aide suivant devra être affiché:

```
./metarkane.sh <word_src> <taille_partie_texte> <out_dir> <texte>...
Éclate un texte en parties égales et le cache dans des fichiers générés à partir de <word_src>
Si la taille du texte n'est pas un multiple de <taille_partie_texte>, le dernier morceau sera complété par des espaces
Les fichiers seront écrits dans le répertoire <out_dir>
```

Lorsque le mauvais nombre d'arguments est entré, vous devrez afficher le message suivant:

`Erreur: pas assez d'arguments`

Lorsque l'argument `word_src` est invalide (i.e. inexistant), vous devrez afficher le message suivant:

`Erreur: le fichier <fichier> n'existe pas`

Où fichier est le chemin renseigné en argument.

Lorsque l'argument `taille_partie_texte` est invalide (i.e. pas un nombre), vous devrez afficher le message suivant:

`Erreur: taille de chaque partie de texte invalide.`

Tous ces messages d'erreur doivent être affichés sur la sortie d'erreur standard.

Chaque fois qu'une erreur est rencontrée, le message d'utilisation devra suivre le message d'erreur, lui aussi sur la sortie d'erreur standard.

Le script devra être propre: il ne crée pas de fichier localement (en dehors du répertoire de sortie), et nettoie après exécution.

Si vous devez créer un fichier temporaire, il devra être situé dans `/tmp`.

## Exemple d'utilisation

Le script s'utilise par exemple de la façon suivante:

```
./metarkane.sh /usr/share/dict/french 3 out ablqe bgfilq ubliufb ewliabvwlebv
```

Cet exemple va séparer la phrase "ablqe bgfilq ubliufb ewliabvwlebv" en parties de 3 octets, génère une liste de fichiers tirés du fichier `/usr/share/dict/french` de façon aléatoire, et place toutes ces parties à la fin de chaque fichier, crées dans un répertoire `out`.
