# Natcalc

Natcalc est un programme d'évaluation d'expressions arithmétiques en langage naturel.

Pour le moment, netcalc ne devra supporter que le français.

## Manuel

Vous devrez réaliser un script shell appelé `netcalc.sh`.

Il devra respecter l'utilisation suivante:

`netcalc.sh <fichier_calc>`

Le fichier calc est un fichier contenant des expressions arithmétiques en langage naturel.

Chaque chiffre sera écrit sous sa forme littérale en français.

Les opérations seront elles aussi écrites sous leur forme littérale.

Les opérateurs que vous devrez supporter seront limités à la multiplication, l'addition, la soustraction, la division et le modulo.

Voici la table de traduction que vous devrez utiliser pour effectuer l'évaluation:


```
zero   | 0
un     | 1
deux   | 2
trois  | 3
quatre | 4
cinq   | 5
six    | 6
sept   | 7
huit   | 8
neuf   | 9
plus   | +
moins  | -
fois   | *
divise | /
modulo | %
```

Le fichier calc que vous devrez lire en entrée devra être lu et interprété.

Il faudra que vous vous assuriez de son existence avant de l'interpréter.

S'il n'est pas présent dans la commande, vous devrez afficher le message suivant:

`Erreur: il manque le chemin vers le fichier d'entrée`

Idem, si le fichier est renseigné, mais n'existe pas, vous devrez afficher le message suivant:

`Erreur: le fichier <fichier> n'existe pas`

Remplacez <fichier> par le fichier renseigné dans la ligne de commande.

Tous ces messages d'erreur doivent être affichés sur la sortie d'erreur standard.

En plus de ça, vous devrez également afficher un manuel d'aide succinct, qui devra respecter le format suivant:

```
natcalc.sh <fichier_calc>
Interprète et évalue un fichier avec des calculs
```

Chaque fois qu'une erreur est rencontrée, le message d'utilisation devra suivre le message d'erreur, lui aussi sur la sortie d'erreur standard.

Le script devra être propre: il ne crée pas de fichier localement (en dehors du répertoire de sortie), et nettoie après exécution.

Si vous devez créer un fichier temporaire, il devra être situé dans `/tmp`.

## Exemple d'utilisation

Soit le fichier `in_calc`, contenant le texte suivant:

```
un deux trois plus neuf huit sept
```

Donc, une fois interprété, vous devrez avoir le résultat suivant: `1110` (123 + 987).
