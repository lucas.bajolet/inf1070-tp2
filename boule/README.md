# Boule Magique 

Dans cette question, vous devez créer un script nommé `boule` qui simulera le jouet 
[Boule magique numéro 8](https://fr.wikipedia.org/wiki/Magic_8-Ball).

## Description du fonctionnement et exemples 

Il y a une unique façon d'utiliser le script 
```sh
./boule [QUESTION]
```
où `[QUESTION]` est une chaine de caractères **obligatoire**. Donc on exécute le script sans
question, on aura un message d'erreur.
```sh
./boule
Il faut une question pour que la magie opère...
```
Si une exécution valide, par exemple `./boule La magie existe vraiment?`, le script choisira 
au **hasard** une réponse parmi les suivantes :
```
Essaye plus tard
Essaye encore
Pas d'avis
C'est ton destin
Le sort en est jeté
Une chance sur deux
Repose ta question
D'après moi oui
C'est certain
Oui absolument
Tu peux compter dessus
Sans aucun doute
Très probable
Oui
C'est bien parti
C'est non
Peu probable
Faut pas rêver
N'y compte pas
Impossible
```
et il l'affichera. 

**Attention** c'est les seules réponses possibles!

L'exécutions de la même commande (poser la même question) plusieurs fois ne donnera pas 
forcément les même réponses.

## Exemples d'exécution
* Exemple 1
```
./boule Est-ce que je réussirai le cours INF1070?
C'est ton destin
./boule "t'es sûre?" 
Oui absolument
./boule "t'es sûre?" 
Le sort en est jeté
```
* Exemple 2
```
./boule Windows est meilleur que Linux...
Faut pas rêver
```
* Exemple 3
```
./boule Neo est toujours dans la matrice? 
D'après moi oui
./boule Neo est toujours dans la matrice? 
Sans aucun doute
./boule Neo est toujours dans la matrice?
Impossible
```

## Contraines et indications
Vous ne pouvez pas utiliser un fichier pour sauvegarder les réponses, elles doivent être
dans le script.

Pensez à `sed` et à des nombres aléatoires.
