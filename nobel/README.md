# Nobel

Votre objectif pour cet exercice est de récupérer la liste des lauréats de prix Nobel en fonction des années et de la catégorie.

Le fichier de référence est disponible publiquement à l'adresse suivante: http://api.nobelprize.org/v1/prize.json

## Manuel

La commande que vous devez ráaliser doit s'appeler `nobel.sh`

Il devra respecter l'utilisation suivante:

`nobel.sh <annee> <categorie>`

L'année doit être un nombre valide, entre 1901 et 2018.

La catégorie n'est pas soumise à une contrainte particulière.

Si un des arguments de la commande est invalide ou absent, le message suivant devra s'afficher:

```
./nobels.sh <annee> <categorie>
Récupère les lauréats d'un prix Nobel et filtre en fonction des critères de recherche.
```

En plus de cette sortie, un message d'erreur sera affiché dépendamment du type d'erreur:

* L'année n'est pas un chiffre: `Erreur: l'année n'est pas un chiffre valide`
* L'année est en dehors des bornes définies: `Erreur: l'année doit être comprise entre 1901 et 2018`

Tous ces messages d'erreur doivent être affichés sur la sortie d'erreur standard.

Chaque fois qu'une erreur est rencontrée, le message d'utilisation devra suivre le message d'erreur, lui aussi sur la sortie d'erreur standard.

Le script doit être propre: aucun fichier temporaire (si besoin) ne doit être présent sur le système après l'exécution du script.o
Si vous devez créer un fichier temporaire, il devra être situé dans `/tmp`.

## Requis

Pour travailler avec les données des prix nobel via la ligne de commande, vous aurez besoin de `jq`.
Il est installable via votre gestionnaire de paquets.

## Exemple d'utilisation

Par exemple, lorsque l'on veut les lauréats des prix Nobel de l'année 2005, dans la catégorie physique:

```sh
$ ./nobels.sh 2005 physics
Roy J. Glauber
John L. Hall
Theodor W. Hänsch
```

Chaque lauréat devra être affiché avec son/ses prénom(s), suivi de son nom.

Le format devra exactement respecter cette convention.
