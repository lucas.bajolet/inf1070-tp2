# hex2c

Pour cet exercice, vous devrez créer un script capable de recevoir des suites de caratcères hexadécimaux, et de les reconvertir en texte.

## Manuel

Le script devra s'appeler `hex2c.sh`.

Il devra respecter l'utilisation suivante:

```
hex2c [-h] <fichier>
hex2c convertit une suite de nombre hexadécimaux en caractères.
Le fichier doit soit être un fichier existant, soit '-' pour lire depuis l'entrée standard

OPTIONS:
-h: mode de compatibilité hexdump. Lit une entrée sous le format généré par 'hexdump'
```

Le script devra valider que le fichier donné en argument est soit un `-`, soit un fichier existant.

Si le fichier est absent, le message suivant devra être affiché: `Erreur: il manque un fichier en argument`

Si un fichier est donné en argument, mais ne respecte pas les contraintes, ce message devra être affiché: `Erreur: fichier <fichier> inexistant`, où `<fichier>` devra être remplacé par celui donné en argument.

Tous ces messages d'erreur doivent être affichés sur la sortie d'erreur standard.

Chaque fois qu'une erreur est rencontrée, le message d'utilisation devra suivre le message d'erreur, lui aussi sur la sortie d'erreur standard.

Le script devra être propre: il ne crée pas de fichier localement (en dehors du répertoire de sortie), et nettoie après exécution.

Si vous devez créer un fichier temporaire, il devra être situé dans `/tmp`.

Un mode de compatibilité hexdump devra également être développé.
Il devra être renseigné sous la forme du fanion `-h`, avant le fichier

## Exemple d'utilisation

Soit le fichier `in_test` donné en complément de cet énoncé:

```
30 31 37 20 41 41 41 20 0A
```

Lorsque donné à votre script, il devra afficher la sortie suivante:

```
$ ./hex2c.sh in_test
017 AAA

```

### Mode hexdump

Pour le mode de compatibilité hexdump, le programme devra prendre en argument du texte tel que généré par hexdump.

Exemple:

```
$ hexdump in_test
0000000 3033 3320 2031 3733 3220 2030 3134 3420
0000010 2031 3134 3220 2030 4130 000a
000001b
```

En donnant cet entrée à votre script, avec le fanion de commande `-h`, vous devrez interpréter ce texte et afficher le texte correspondant.

Vous pourrez tester ce mode avec tout fichier texte valide.

Exemple:

```
$ hexdump in_test | ./hex2c -h -
30 31 37 20 41 41 41 20 0A
```
