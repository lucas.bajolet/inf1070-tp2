# Gestionnaire de tâches simple (_TODO List_)

L’expression _todo list_ est un anglicisme signifiant liste de tâches. Il s’agit 
d’un document simplifié énumérant des choses à faire. 

Dans cette question, vous devez créer un script nommé `todo` qui permet de gérer
une liste de tâches simple, grâce à trois fonctionnalités.

## Description du fonctionnement et exemples 

### Définitions
* Une _tâche_ est une phrase simple (une chaine de caractères), sans saut de ligne, 
décrivant une activité à effectuer.

* Une _liste de tâches_ est un fichier texte avec le nom `todo_liste` qui sauvegarde 
l'ensemble des tâches à effectuer, une par ligne. Son emplacement est toujours dans 
le répertoire `HOME` de l'utilisateur courant : `~/todo_liste`

* Une _fonctionnalité_ indique au script `todo` quel comportement il devra exécuter, 
exactement comme les options d'une commande. 

### Fonctionnalités

Le script `todo` peut s'exécuter uniquement avec l'une des fonctionnalités suivantes :

#### Ajouter une tâche : `ajouter`
Cette fonctionnalité a un double rôles :
* si le fichier `todo_liste` n'existe pas, le script va le créer;
* ajouter la tâche saisie à la fin du fichier.

Par exemple, si on exécute la commande suivante :
```sh
./todo ajouter "Sortir le chien avant 17h"
```
le script `todo` va vérifier si le fichier `~\todo_liste` existe. Si oui, il va ajouter
la tâche `Sortir le chien avant 17h` à la fin du fichier; si non, le fichier sera créé 
avec la tâche `Sortir le chien avant 17h`.

Si aucune tâche n'a été spécifiée, le script ne fera rien. Autrement dit, s'il reçoit un
seul argument (la fonctionnalité `ajouter` seulement), alors le script ne fait rien.

#### Afficher la liste des tâches : `afficher`
Cette fonctionnalité vérifie si un fichier `todo_liste` existe, si oui elle affichera
son contenu. Si le fichier `todo_liste` n'existe pas, le script affiche le message suivant :
```
Aucune liste de tâches n'est disponible!
```
Par exemple, une première utilisation du script `todo` donnera,
```
./todo  afficher
Aucune liste de tâches n'est disponible!
```
Si on ajoute quelques tâches avec la fonctionnalité `ajouter`,
```sh
./todo ajouter "Sortir le chien avant 17h"
./todo ajouter "Vérifier les courriels" 
./todo ajouter "Commencer le TP2..." 
```
alors l'exécution du script `todo` avec la fonctionnalité `afficher` donnera
```sh
./todo afficher
    1  Sortir le chien avant 17h 
    2  Vérifier les courriels   
    3  Commencer le TP2...
```
Noter que cette fonctionnalité ne prend aucun argument en plus de `afficher`, 
par conséquent, tout ce qui vient après, sera ignoré par le script.

#### Supprimer une tâche : `supprimer`
Cette fonctionnalité supprime une tâche de `todo_liste` grâce à son numéro.

Si on a la liste de tâches suivante :
```sh
./todo afficher
    1  Sortir le chien avant 17h 
    2  Vérifier les courriels   
    3  Commencer le TP2...
```
alors la commande suivante :
```sh
./todo supprimer 2
```
supprimera la tâche `Vérifier les courriels` et la liste des tâches deviendra
```sh
./todo afficher
    1  Sortir le chien avant 17h 
    2  Commencer le TP2...
```

Si on essaie de supprimer une tâche, mais que le fichier `todo_liste` n'existe pas, 
le message suivant sera afficher
```
./todo supprimer 1
Aucune liste de tâches n'est disponible!
```
Si on donne le numéro d'une tâche qui n'existe pas, le script ne fera rien.
```sh
./todo afficher
    1  Sortir le chien avant 17h 
    2  Commencer le TP2...
./todo supprimer 4
./todo afficher
    1  Sortir le chien avant 17h 
    2  Commencer le TP2...
```
Si on supprime l'unique tâche d'une liste, alors le fichier `todo_liste` sera supprimé.
```
./todo afficher
    1  Sortir le chien avant 17h 
./todo supprimer 1
./todo afficher
Aucune liste de tâches n'est disponible!
```

Si le paramètre saisi après la fonctionnalité `supprimer` n'est pas un chiffre, le message
d'erreur suivant sera afficher (sur le canal d'erreur) :
```
./todo supprimer a
Il faut saisir un paramètre numérique!
```

**Remarque.** Il faut impérativement que l'une des fonctionnalités `afficher`, `ajouter` 
ou `supprimer` soit donnée à chaque appel au script `todo`. Sinon un message d'erreur est
affiché.
```
./todo add "Pas comme ça"
Aucune fonctionnalité n'a été précisée!
./todo show
Aucune fonctionnalité n'a été précisée!
./todo
Aucune fonctionnalité n'a été précisée!
```

## Exemple d'exécution
On suppose qu'on n'a aucune liste de tâches créé. On a alors les exécutions suivantes,
```
./todo afficher
Aucune liste de tâches n'est disponible!
./todo add "Faut bien lire l'énoncé avant de commencer..." 
Aucune fonctionnalité n'a été précisée!
./todo ajouter "Faut bien lire l'énoncé avant de commencer..." 
./todo afficher
    1  Faut bien lire l'énoncé avant de commencer...   
./todo "Ensuite, faut relire le cours"
Aucune fonctionnalité n'a été précisée!
./todo ajouter "Ensuite, faut relire le cours"
./todo ajouter "Faire des recherches s'il le faut"
./todo ajouter "Demander aux prof et démonstrateurs"
./todo afficher
    1  Faut bien lire l'énoncé avant de commencer...
    2  Ensuite, faut relire le cours
    3  Faire des recherches s'il le faut
    4  Demander aux prof et démonstrateurs
./todo supprimer 5
./todo afficher
    1  Faut bien lire l'énoncé avant de commencer...
    2  Ensuite, faut relire le cours
    3  Faire des recherches s'il le faut
    4  Demander aux prof et démonstrateurs
./todo supprimer 1
./todo afficher
    1  Ensuite, faut relire le cours
    2  Faire des recherches s'il le faut
    3  Demander aux prof et démonstrateurs
./todo supprimer 2
./todo afficher
    1  Ensuite, faut relire le cours
    2  Demander aux prof et démonstrateurs
./todo supprimer 2
./todo supprimer 2
./todo afficher
    1  Ensuite, faut relire le cours
./todo supprimer 1
./todo afficher
Aucune liste de tâches n'est disponible!
```

## Contraintes et indications
* C'est le script qui doit gérer la numérotation des tâches et non l'utilisateur.
* L'appel à la fonctionnalité `ajouter` sera toujours testé avec des tâches. Vous donc
pouvez supposer qu'il y aura toujours une chaine de caractères qui suivra `ajouter`.
* L'appel à la fonctionnalité `supprimer` sera toujours testé avec des nombres. Vous 
pouvez donc assumer que ce qui suit `supprimer` est toujours un chiffre.

`sed` vous aidera! 
